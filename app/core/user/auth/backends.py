from django.contrib.auth.backends import ModelBackend as BaseModelBackend
from rest_framework import exceptions


class ModelBackend(BaseModelBackend):

    def authenticate(self, request, username=None, password=None, organization_id=None, **kwargs):
        user = super(ModelBackend, self).authenticate(request, username, password, **kwargs)

        if user is None:
            return None

        if user.is_superuser and user.is_staff:
            return user

        if organization_id is None:
            raise exceptions.NotAuthenticated(detail='Organization id were not provided')

        if organization_id not in user.organization.all().values_list('id', flat=True):
            raise exceptions.PermissionDenied()

        return user

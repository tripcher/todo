from rest_framework import serializers


class AuthSerializer(serializers.Serializer):
    username = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)
    organization_id = serializers.IntegerField(required=True)

from rest_framework.authentication import \
    TokenAuthentication as BaseTokenAuthentication

from app.core.user import models


class TokenAuthentication(BaseTokenAuthentication):
    model = models.Token

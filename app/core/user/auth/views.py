from rest_framework import generics, response, status

from app.core.user import serializers as user_serializers
from app.core.user.auth import serializers as auth_serializers
from app.core.user.auth import services


class AuthViews(generics.GenericAPIView):

    serializer_class = auth_serializers.AuthSerializer

    # TODO: добавить удаление токена при выходе

    def post(self, request) -> response.Response:

        """
            Вход - возвращает токен и пользователя, если успешно
        """

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            user, token = services.authorization(request, **serializer.data)

            return response.Response(
                {
                    'token': token.key,
                    'user': user_serializers.UserSerializer(user).data
                },
                status=status.HTTP_200_OK
            )

from django.contrib.auth import authenticate
from rest_framework import exceptions

from app.core.user import models


def authorization(request=None, **kwargs) -> tuple:
    user = authenticate(request=request, **kwargs)

    if user is None:
        raise exceptions.AuthenticationFailed()

    organization = models.OrganizationUser.objects.get(id=kwargs.get('organization_id'))

    token, _ = models.Token.objects.get_or_create(user=user, organization=organization)

    return user, token

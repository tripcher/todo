import binascii
import os

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework.authtoken.models import Token as BaseToken


class OrganizationUser(models.Model):

    """Модель организации"""

    title = models.CharField(_('Title'), max_length=255)

    def __unicode__(self):
        return str(self.title)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')


class User(AbstractUser):

    """Модель пользователя"""

    email = models.EmailField(_('email address'), unique=True)
    organization = models.ManyToManyField(OrganizationUser, blank=True, related_name='users')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return str(self.email)

    def __str__(self):
        return str(self.email)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')


class Token(BaseToken):
    key = models.CharField(_("Key"), max_length=40, primary_key=True)
    user = models.OneToOneField(get_user_model(), related_name='auth_token', on_delete=models.CASCADE,
                                verbose_name=_("User"))
    organization = models.OneToOneField(OrganizationUser, related_name='auth_token', on_delete=models.CASCADE,
                                        verbose_name=_("Organization"))
    created = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        verbose_name = _("Token")
        verbose_name_plural = _("Tokens")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key

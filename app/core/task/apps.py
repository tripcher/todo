from django.apps import AppConfig


class TaskConfig(AppConfig):
    name = 'app.core.task'

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .models import Task
from .permissions import TaskPermission
from .serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    permission_classes = (IsAuthenticated, TaskPermission,)
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, organization_id=self.request.auth.organization.id)

    def get_queryset(self):
        qs = super(TaskViewSet, self).get_queryset()
        return qs.filter(organization_id=self.request.auth.organization.id)

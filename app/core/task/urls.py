from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'', views.TaskViewSet, basename='task_list')
urlpatterns = router.urls

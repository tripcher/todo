from rest_framework import serializers

from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'title', 'text', 'user', 'organization', 'date_created')
        read_only_fields = ('id', 'user', 'organization', 'date_created')

from rest_framework import permissions


class TaskPermission(permissions.BasePermission):

    """Проверка прав для задачи - пользователь может изменять только свои задачи"""

    def has_object_permission(self, request, view, obj):
        if request.method not in permissions.SAFE_METHODS:
            return obj.user == request.user
        return True

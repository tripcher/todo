from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

from app.core.user.models import OrganizationUser


class Task(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    text = models.TextField('Описание', blank=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='tasks')
    organization = models.ForeignKey(OrganizationUser, on_delete=models.CASCADE)
    date_created = models.DateTimeField('Дата создания', default=timezone.now)

    def __unicode__(self):
        return str(self.title)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'

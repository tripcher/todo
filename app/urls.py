from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from app.core.user.auth import views as auth_views

patterns = [
    path('tasks/', include('app.core.task.urls')),
    path('auth/', auth_views.AuthViews.as_view(), name='login')
]

schema_view = get_schema_view(
   openapi.Info(
      title="Todo",
      default_version='v1',
      description="Test todo",
      contact=openapi.Contact(email="stanislav.shkitin@yandex.ru")
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('admin/', admin.site.urls),
    path('api/v1/', include(patterns)),
]

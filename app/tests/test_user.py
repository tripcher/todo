import pytest
from rest_framework import exceptions

from .test_fixture import *


def test_auth_service_valid_authorization(user, organization):
    from app.core.user.auth import services

    data = {'username': 'test@example.com', 'password': 'password',
            'organization_id': organization.id}

    user, token = services.authorization(**data)

    assert user.email == 'test@example.com'
    assert token.organization_id == organization.id


@pytest.mark.xfail(raises=exceptions.PermissionDenied)
def test_auth_service_invalid_organization_authorization(user, organization_extra):
    from app.core.user.auth import services

    data = {'username': 'test@example.com', 'password': 'password',
            'organization_id': organization_extra.id}

    services.authorization(**data)


@pytest.mark.xfail(raises=exceptions.AuthenticationFailed)
def test_auth_service_invalid_password_authorization(user, organization_extra):
    from app.core.user.auth import services

    data = {'username': 'test@example.com', 'password': 'nopassword',
            'organization_id': organization_extra.id}

    services.authorization(**data)


@pytest.mark.xfail(raises=exceptions.AuthenticationFailed)
def test_auth_service_invalid_username_authorization(user, organization_extra):
    from app.core.user.auth import services

    data = {'username': 'notest@example.com', 'password': 'nopassword',
            'organization_id': organization_extra.id}

    services.authorization(**data)


def test_auth_login_valid(api_client, user, organization):
    response = api_client.post('/api/v1/auth/', {'username': 'test@example.com', 'password': 'password',
                               'organization_id': organization.id}, format='json')
    assert response.status_code == 200
    assert response.data.get('user') == {'id': user.id, 'first_name': 'Имя', 'last_name': 'Фамилия',
                                         'email': 'test@example.com'}


@pytest.mark.django_db
def test_auth_login_invalid_organization(api_client, user, organization_extra):
    response = api_client.post('/api/v1/auth/', {'username': 'test@example.com', 'password': 'password',
                               'organization_id': organization_extra.id}, format='json')
    assert response.status_code == 403


@pytest.mark.django_db
def test_auth_login_invalid_username(api_client, organization):
    response = api_client.post('/api/v1/auth/', {'username': 'notest@example.com', 'password': 'password',
                               'organization_id': organization.id}, format='json')
    assert response.status_code == 401


@pytest.mark.django_db
def test_auth_login_invalid_password(api_client, organization):
    response = api_client.post('/api/v1/auth/', {'username': 'test@example.com', 'password': 'nopassword',
                               'organization_id': organization.id}, format='json')
    assert response.status_code == 401


@pytest.mark.django_db
def test_auth_permission_false(api_client):
    response = api_client.get('/api/v1/tasks/')
    assert response.status_code == 401


@pytest.mark.django_db
def test_auth_permission_true(api_auth_client):
    response = api_auth_client.get('/api/v1/tasks/')
    assert response.status_code == 200

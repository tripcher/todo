import pytest

from .test_fixture import *


def test_tasks_get(api_auth_client, tasks):
    response = api_auth_client.get('/api/v1/tasks/')
    assert len(response.data) == 2


def test_task_create(api_auth_client):
    from app.core.task.models import Task
    response = api_auth_client.post('/api/v1/tasks/', {'title': 'Task test', 'text': 'text'}, format='json')
    assert response.status_code == 201
    assert Task.objects.count() == 1


def test_task_create_values(api_auth_client, user, organization):
    from app.core.task.models import Task
    api_auth_client.post('/api/v1/tasks/', {'title': 'Task test', 'text': 'text'}, format='json')
    task = Task.objects.get(user=user, organization=organization)
    assert task.title == 'Task test'
    assert task.text == 'text'


def test_task_get(api_auth_client, user, tasks, organization):
    response = api_auth_client.get('/api/v1/tasks/{}/'.format(tasks[0].id))
    assert response.status_code == 200
    assert response.data['id'] == tasks[0].id
    assert response.data['user'] == user.id
    assert response.data['organization'] == organization.id


def test_task_get_organization_extra(api_auth_client, tasks):
    response = api_auth_client.get('/api/v1/tasks/{}/'.format(tasks[1].id))
    assert response.status_code == 404


def test_task_put(api_auth_client, tasks):
    response = api_auth_client.put('/api/v1/tasks/{}/'.format(tasks[0].id), {'title': 'put title', 'text': 'put text'},
                                   format='json')
    assert response.status_code == 200
    assert response.data['title'] == "put title"
    assert response.data['text'] == "put text"


def test_task_put_organization_extra(api_auth_client, tasks):
    response = api_auth_client.put('/api/v1/tasks/{}/'.format(tasks[1].id), {'title': 'put title', 'text': 'put text'},
                                   format='json')
    assert response.status_code == 404


def test_task_put_user_extra(api_auth_client, tasks):
    response = api_auth_client.put('/api/v1/tasks/{}/'.format(tasks[2].id), {'title': 'put title', 'text': 'put text'},
                                   format='json')
    assert response.status_code == 403


def test_task_delete(api_auth_client, tasks):
    response = api_auth_client.delete('/api/v1/tasks/{}/'.format(tasks[0].id))
    assert response.status_code == 204


def test_task_delete_organization_extra(api_auth_client, tasks):
    response = api_auth_client.delete('/api/v1/tasks/{}/'.format(tasks[1].id))
    assert response.status_code == 404


def test_task_delete_user_extra(api_auth_client, tasks):
    response = api_auth_client.delete('/api/v1/tasks/{}/'.format(tasks[2].id))
    assert response.status_code == 403

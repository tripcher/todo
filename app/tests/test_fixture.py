import pytest


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient

    return APIClient()


@pytest.fixture
@pytest.mark.django_db
def organizations():

    """Создает лист организаций из 2 элементов"""

    from app.core.user.models import OrganizationUser
    organization1 = OrganizationUser.objects.create(title='organization 1')
    organization2 = OrganizationUser.objects.create(title='organization 2')
    return [organization1, organization2]


@pytest.fixture
@pytest.mark.django_db
def organization(organizations):
    return organizations[0]


@pytest.fixture
@pytest.mark.django_db
def organization_extra(organizations):
    return organizations[1]


@pytest.fixture
@pytest.mark.django_db
def user(django_user_model, organizations):

    """
    Создает юзера

    username = test
    email = test@example.com
    password = password
    """

    UserModel = django_user_model
    extra_fields = {
        'first_name': 'Имя',
        'last_name': 'Фамилия'
    }
    user = UserModel._default_manager.create_user(
        'test', 'test@example.com', 'password', **extra_fields
    )
    user.organization.add(organizations[0])
    return user


@pytest.fixture
@pytest.mark.django_db
def user_extra(django_user_model, organizations):

    """
    Создает юзера

    username = test_extra
    email = test_extra@example.com
    password = password
    """

    UserModel = django_user_model
    extra_fields = {
        'first_name': 'Имя',
        'last_name': 'Фамилия'
    }
    user = UserModel._default_manager.create_user(
        'test_extra', 'test_extra@example.com', 'password', **extra_fields
    )
    user.organization.add(organizations[0])
    return user


@pytest.fixture
@pytest.mark.django_db
def tasks(user, user_extra, organization, organization_extra):

    """
    Создает лист задач из 3 элементов

    1 задача принадлежат организации, 2 другой организации, 3 другому пользователю
    """

    from app.core.task.models import Task
    task1 = Task.objects.create(title='Task 1', text='text', user_id=user.id, organization_id=organization.id)
    task2 = Task.objects.create(title='Task 2', text='text', user_id=user.id, organization_id=organization_extra.id)
    task3 = Task.objects.create(title='Task 3', text='text', user_id=user_extra.id, organization_id=organization.id)
    return [task1, task2, task3]


@pytest.fixture
@pytest.mark.django_db
def api_auth_client(user, organization):
    from rest_framework.test import APIClient
    from app.core.user.models import Token

    token, _ = Token.objects.get_or_create(user=user, organization=organization)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    return client

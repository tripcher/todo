FROM python:alpine3.7

ENV PYTHONUNBUFFERED 1

RUN mkdir /work

WORKDIR /work

COPY requirements.txt /work/

RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

COPY . /work/

RUN chmod +x /work/runserver.sh

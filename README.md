# ToDo

##Task
Реализовать REST API для клиентского приложения, которое имеет следующие функции:
1) Авторизация пользователя под разными организациями (один пользователь может состоять в нескольких организациях).
2) Получить список задач под авторизованным пользователем, выводятся только те задачи,
 которые принадлежат организации, в которой в данный момент пользователь авторизован.
3) Добавить задачу под авторизованным пользователем
4) Редактировать или удалить задачу под авторизованным пользователем
 (можно изменять только свои задачи)
5) Получить задачу по id под авторизованным пользователем (доступны только задачи текущей организации)


##Used stack
- Python
- Django
- Django Rest Framework
- PostgreSQL
- Docker

##Installation process
```
git clone https://tripcher@bitbucket.org/tripcher/todo.git

cd ./todo
docker-compose build
docker-compose up

```

###Create super user
```
docker exec -it project_todo python3 manage.py createsuperuser
```

##Run PyTest
```
docker exec -it project_todo pytest --flake8 --isort
```